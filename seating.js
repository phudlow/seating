var Seating = function (rows, cols) {


	var _rows = rows; 

	var getRows = function () {

		return _rows;
	}

	var _cols = cols;

	var getColumns = function () {

		return _cols;
	}

	var _newLayout = function () {
		var twoDArray = [];
		for ( var i = 0 ; i < rows ; i++ ) {
			twoDArray[i] = [];
			for ( var j = 0 ; j < cols ; j++ ) {
				twoDArray[i][j] = false;
			} 
		}
		return twoDArray;
	};

	var _layout = _newLayout();

	var getLayout = function () {

		return _layout;
	}

	var _reservedSeats = [];

	var getReservedSeats = function () {

		return _reservedSeats;
	}

	var _rankOf = function (seat) {

		return Number(seat.match(/[0-9]+(?=C)/)) + Math.abs(_cols / 2  + 0.5 - seat.match(/[0-9]+$/));
	};

	var _validateSeat = function (seat) {

		// Seat format validation
		if ( !seat.match(/[R][0-9]+[C][0-9]+/) ) {
			throw new Error("Seat \"" + seat + "\" is not a valid seat format.  No reservations made.");
		}

		// Check seat is already taken
		if ( _layout[seat.match(/[0-9]+(?=C)/) - 1][seat.match(/[0-9]+$/) - 1] === true ) {
			throw new Error("Seat \"" + seat + "\" is already taken.  No reservations made.");
		} 
	};

	var findBestSeats = function (n) {

		// Validation
		if ( n === undefined ) {
			throw new Error("findBestSeats was called with an undefined argument");
		}		
		if ( typeof n !== 'number' ) {
			throw new TypeError("findBestSeats was called with a non-number argument: " + n); 
		}
		if ( n < 1 || n > _cols ) {
			throw new RangeError("findBestSeats was called with an invalid seat quantity: " + n);
		}


		// An array containing the best seats so far in the search
		var bestSeats = [];

		// Sum of the ranks of these seats (a lower rank sum means better seats)
		var bestSeatsRankSum = Infinity;

		// FOR EACH ROW
		for ( var i = 0 ; i < _rows ; i++ ) {

			// The best ranked seat INDEX in this currently-being-searched collection of open seats
			var thisBestSeatIndex = -1;

			// The seat numbers in this in this currently-being-searched collection of open seats	
			var thisSeats = [];


			// FOR EACH SEAT
			for ( var j = 0 ; j < _cols ; j++ ) {

				// IF seat is AVAILABLE, push to thisSeats, & update bestSeatRankIndex if it is the best seat
				if (_layout[i][j] === false) {
					var lastSeat = ("R" + ( i + 1 ) + "C" + ( j + 1 ));

					thisSeats.push(lastSeat);

					if ( thisBestSeatIndex === -1 || 
						_rankOf(lastSeat, _cols) < _rankOf(thisSeats[thisBestSeatIndex], _cols) ) {
						thisBestSeatIndex = thisSeats.length - 1;
					}
				}


				// IF seat is RESERVED & thisSeats is less than the requested, restart array
				if ((_layout[i][j] === true || j === _cols - 1) && (thisSeats.length < n)) {
					thisSeats = [];
					thisBestSeatIndex = -1;
				}


				// IF seat is RESERVED & thisSeats is equal to or greater than requested number
				if ((_layout[i][j] === true || j === _cols - 1) && (thisSeats.length >= n)) {

					// WHILE thisSeats is greater than the number requested, TRIM the worst ranked seat from thisSeats
					while (thisSeats.length > n) {

						// IF bestINDEX < thisSeats.length / 2, Get rid of the RIGHT/GREATER index
						if (thisBestSeatIndex < thisSeats.length / 2) { 
							thisSeats.pop();
						}

						// IF bestINDEX >= thisSeats.length / 2, Get rid of the LEFT/LESSER index
						else if (thisBestSeatIndex >= thisSeats.length / 2) {
							thisSeats.shift();
							thisBestSeatIndex--;
						}
					}

					if ( thisSeats.length > 0 ) {

						// Calculate the sum of the ranks of the thisSeats array
						var thisBestSeatRankSum = 0;
						for ( var k = 0  ; k < thisSeats.length ; k++ ) {
							thisBestSeatRankSum += _rankOf(thisSeats[k], _cols);
						}

						// Assign bestSeatsRankSum to thisBestSeatsRankSum if better
						if ( thisBestSeatRankSum < bestSeatsRankSum ) {
							bestSeatsRankSum = thisBestSeatRankSum;
							bestSeats = thisSeats;
						}
					}
					thisSeats = [];
					thisBestSeatIndex = -1;
				}
			}
		}

		// IF seats are not available in the requested amount, return FALSE, else return bestSeats
		if ( bestSeats.length === 0 ) {
			return false;
		} else {
			return bestSeats;
		}
	};

	var reserve = function (seats) {

		if ( seats == 0 ) {
			throw new Error("findBestSeats was called with a falsey/missing argument: " + seats);
		}
		if ( typeof seats === "string" ) {
			_validateSeat(seats);
			_layout[seats.match(/[0-9]+(?=C)/) - 1][seats.match(/[0-9]+$/) - 1] = true;
			_reservedSeats = _reservedSeats.concat(seats);
		} else {
			for ( var i = 0 ; i < seats.length ; i++ ) {
				_validateSeat(seats[i]);
			}
			for ( var i = 0 ; i < seats.length ; i++ ) {
				_layout[seats[i].match(/[0-9]+(?=C)/) - 1][seats[i].match(/[0-9]+$/) - 1] = true;
				_reservedSeats = _reservedSeats.concat(seats[i]);
			}
		}
	};

	var buildSeatingHTML = function () {

		var reservedSeat = "<div class=\"seat reserved\">";
		var openSeat = "<div class=\"seat open\">";	

		var seatingHTMLString = "<div class=\"seating_section\">";

		for ( var i = 0 ; i < _layout.length ; i++ ) {

			seatingHTMLString += "<div class=\"seating_row\">"

			for ( var j = 0 ; j < _layout[i].length ; j++ ) {

				var seatNumber = "" + ("R" + ( i + 1 ) + "C" + ( j + 1 ));

				if ( _layout[i][j] === true ) {
					seatingHTMLString += reservedSeat;

				}

				if ( _layout[i][j] === false ) {
					seatingHTMLString += openSeat;
				}

				seatingHTMLString += seatNumber;
				seatingHTMLString += '</div>';
			}

			seatingHTMLString += "</div>";
		}

	    var seatingHTMLDOM = document.createElement('div');
	    seatingHTMLDOM.innerHTML = seatingHTMLString;
	    return seatingHTMLDOM.childNodes[0];	
	};

	// Expose public methods
	return {
		findBestSeats: findBestSeats,
		reserve: reserve,
		getRows: getRows,
		getColumns: getColumns,
		getLayout: getLayout,
		getReservedSeats: getReservedSeats,
		buildSeatingHTML: buildSeatingHTML
	};
};