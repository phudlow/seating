#Seating

A simple library providing useful tools for auditorium-style seating scenarios.

##Getting Started

Call the Seating constructor with desired rows and columns (rows-per-seat):

```javascript
var section = new Seating(rows, columns);
```


Specific seats are referenced as a string in the format: **'R#C#'**, where the first # corresponds to the row number, and the second # corresponds to the column number.

For example, 'R2C3' refers to the third seat in the second row.


##Methods

###getRows()
Returns the number of rows in this instance of Seating.
###getColumns()
Returns the seats-per-row in this instance of Seating.
###getLayout()
Returns 2D array representing the seating layout, populated with boolean values representing seat reservation.
###getReservedSeats()
Returns array containing currently reserved seats.
###findBestSeats(number)
Takes a number and returns an array containing the best available block of seats based on the Manhattan distance from the front center seat.  The seats must be adjacent and in the same row.  Returns `false` if unavailable.
###reserve(seats)
Takes a seat or array of seats and makes those seats reserved.
###buildSeatingHTML()
Returns HTML representing the seating layout with current reservations.  Orientation assumes the stage is directly above.

Example with a 2x2 seating layout with the front row reserved:

```html
<div class="seating_section">
    <div class="seating_row">
        <div class="seat reserved">R1C1</div>
        <div class="seat reserved">R1C2</div>
    </div>
    <div class="seating_row">
        <div class="seat open">R2C1</div>
        <div class="seat open">R2C2</div>
    </div>
</div>
```

##Planned Features

 + Data-binding between Seating data and live Seating HTML.
 + Seating HTML interactability.
 + Customizable orientation.
 + Support for multiple Seating sections/blocks.